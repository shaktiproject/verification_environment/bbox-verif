import random
import sys
import cocotb
import logging as log
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge
from cocotb.monitors import BusMonitor
from cocotb.drivers import BusDriver
from cocotb.binary import BinaryValue
from cocotb.regression import TestFactory
from cocotb.scoreboard import Scoreboard
from cocotb.result import TestFailure
from cocotb.clock import Clock
from cocotb_coverage import crv
from cocotb_coverage import coverage

from model_mkbitmanip import *
from constants import *

class InputDriver(BusDriver):
    """Drives inputs to DUT."""
    _signals = ["mav_putvalue_instr", "mav_putvalue_src1",
                "mav_putvalue_src2", "mav_putvalue_src3", "EN_mav_putvalue"]

    def __init__(self, dut):
        BusDriver.__init__(self, dut, None, dut.CLK)


class InputTransaction(object):
    """Transactions to be sent by InputDriver"""
    def __init__(self, tb, mav_putvalue_instr=0, mav_putvalue_src1=0,
                    mav_putvalue_src2=0, mav_putvalue_src3=0,
                    EN_mav_putvalue=0):
        self.mav_putvalue_instr = BinaryValue(mav_putvalue_instr, tb.mav_putvalue_instr_bits, False)
        self.mav_putvalue_src1 = BinaryValue(mav_putvalue_src1, tb.mav_putvalue_src1_bits, False)
        self.mav_putvalue_src2 = BinaryValue(mav_putvalue_src2, tb.mav_putvalue_src2_bits, False)
        self.mav_putvalue_src3 = BinaryValue(mav_putvalue_src3, tb.mav_putvalue_src3_bits, False)
        self.EN_mav_putvalue = BinaryValue(EN_mav_putvalue, tb.EN_mav_putvalue_bits, False)

class InputMonitor(BusMonitor):
    """ Passive monitors of DUT."""
    _signals = ["mav_putvalue_instr", "mav_putvalue_src1",
                "mav_putvalue_src2","mav_putvalue_src3", "EN_mav_putvalue"]

    def __init__(self, dut, callback=None, event=None):
        BusMonitor.__init__(self, dut, None, dut.CLK, dut.RST_N,
                            callback=callback, event=event)
        self.name = "in"

    @coroutine
    def _monitor_recv(self):
        clkedge = RisingEdge(self.clock)

        while True:
            yield clkedge
            vec = ( self.bus.mav_putvalue_instr.value.integer,
                    self.bus.mav_putvalue_src1.value.integer,
                    self.bus.mav_putvalue_src2.value.integer,
                    self.bus.mav_putvalue_src3.value.integer,
                    self.bus.EN_mav_putvalue.value.integer)
            self._recv(vec)

class OutputTransaction(object):
    """Transaction to be expected / received by OutputMonitor."""

    def __init__(self, tb=None, mav_putvalue=0):
        """For expected transactions, value 'None' means don't care.
        tb must be an instance of the Testbench class."""
        if mav_putvalue is not None and isinstance(mav_putvalue, int):
            mav_putvalue = BinaryValue(mav_putvalue, tb.mav_putvalue_bits, False)

        self.value = (mav_putvalue)


class OutputMonitor(BusMonitor):
    """Observes outputs of DUT."""
    _signals = ["mav_putvalue","EN_mav_putvalue"]

    def __init__(self, dut, tb, callback=None, event=None):
        BusMonitor.__init__(self, dut, None, dut.CLK, dut.RST_N, callback=callback, event=event)
        self.name = "out"
        self.tb = tb

    @coroutine
    def _monitor_recv(self):
        clkedge = RisingEdge(self.clock)
        while True:
            yield clkedge
#            print(" dut mav_putvalue :",self.bus.mav_putvalue.value)
            recieved_mav_putvalue = self.bus.mav_putvalue.value
            self._recv(OutputTransaction(self.tb, recieved_mav_putvalue))

class Testbench(object):
    class MyScoreboard(Scoreboard):
        def compare(self, got, exp, log, **_):
            got_output = got.value
            exp_output = exp.value
            exp_valid_bit = exp_output  & 1
            got_valid_bit = got_output  & 1
#            log.info("Len exp: {0!s} got: {1!s}".format((exp_output), (got_output)))
#            log.info("INVALID exp: {0!s} got: {1!s}".format(exp_valid_bit, got_valid_bit))
            if got_valid_bit != exp_valid_bit:
                log.warning("ERROR Valid Bit differs Expected: {0!s}. differ Received: {1!s}.".format( hex(exp_output), hex(got_output)))
                fh.write("Valid Bit differs  Expected: {0!s}. differ Received: {1!s}.".format( hex(exp_output), hex(got_output)))
                exit(1)
            else:
                if exp_valid_bit == 0:
                    fh.write('passed\n')
                    log.warning('valid bit pass')

                else: 
                    if got_output != exp_output:
                        log.warning("ERROR Output differs Expected: {0!s}. differ Received: {1!s}.".format( hex(exp_output), hex(got_output)))
                        fh.write("Output differs Expected: {0!s}. differ Received: {1!s}.".format( hex(exp_output), hex(got_output)))
                        exit(1)
                    else:
                        fh.write('passed\n')
                        log.warning('output bit pass')


    def __init__(self, dut):
        self.dut = dut
        self.stopped = False
        self.mav_putvalue_instr_bits = 32
        self.mav_putvalue_src1_bits = 32
        self.mav_putvalue_src2_bits = 32
        self.mav_putvalue_src3_bits = 32
        self.EN_mav_putvalue_bits = 1
        self.mav_putvalue_bits = 33
        self.input_mon = InputMonitor(dut, callback=self.model)

        init_val = OutputTransaction(self)

        self.input_drv = InputDriver(dut)
        self.output_mon = OutputMonitor(dut, self)

        # scoreboard on the outputs
        self.expected_output = []
        self.scoreboard = Testbench.MyScoreboard(dut)
        self.scoreboard.add_interface(self.output_mon, self.expected_output)


        #self.input_mon = InputMonitor(dut, callback=self.model)

    def model(self, transaction):
        """Model """
        mav_putvalue_instr, mav_putvalue_src1, mav_putvalue_src2, mav_putvalue_src3, EN_mav_putvalue = transaction
        #mav_putvalue = 0
        mav_putvalue = bitmanip(mav_putvalue_instr, mav_putvalue_src1, mav_putvalue_src2, mav_putvalue_src3, EN_mav_putvalue)
        print("-- instr: {0} src1: {1} src2: {2} src3: {3} model: {4} ".format(hex(mav_putvalue_instr), hex(mav_putvalue_src1), hex(mav_putvalue_src2), hex(mav_putvalue_src3), hex(mav_putvalue)))
        self.expected_output.append( OutputTransaction(self, mav_putvalue) )

    def stop(self):
        """
        Stop generation of expected output transactions.
        One more clock cycle must be executed afterwards, so that, output of
        D-FF can be checked.
        """
        self.stopped = True


#--------------------------------------------------------------------------------------------------------------------------------
def random_input_gen(tb,n=200):

    #SLO function
    #instr: 0x48007033 src1: 0x9669fe4 src2: 0x846aaba src3: 0xc6e471c model: 0x9669fe4
    #Expected: 0x9669fe4. differ Received: 0x966aea4
    for i in range(n):
        mav_putvalue_instr = 0x48007033
        mav_putvalue_src1 =0x9669fe4
        mav_putvalue_src2 =0x846aaba
        mav_putvalue_src3 =0xc6e471c
        EN_mav_putvalue = 1

        mav_putvalue_instr = 0x2cfa1393 
        mav_putvalue_src1 =  0x1e5a1b3d
        mav_putvalue_src2 =  0xf3ee45d1
        mav_putvalue_src3 = 0x4bea8893
        EN_mav_putvalue = 1
#
#    mav_putvalue_instr  =  0x48007033
#    mav_putvalue_src1  =  0xc70e9ba
#    mav_putvalue_src2  =  0x9e0b801
#    mav_putvalue_src3  =  0xb916770
#    EN_mav_putvalue  =  1
#
#    # fail
#    mav_putvalue_instr = 0x4005033
#    mav_putvalue_src1  = 0x395a30b0
#    mav_putvalue_src2  = 0x5c2b38e3
#    mav_putvalue_src3  = 0x5f9dbf30
#    EN_mav_putvalue  =    1

#    mav_putvalue_instr = 0x8005033
#    mav_putvalue_src1 =0x964e7cb
#    mav_putvalue_src2 =0x81c5f9f
#    mav_putvalue_src3 =0x4320b1
#    EN_mav_putvalue = 1

#    mav_putvalue_instr= 0x23aad59b
#    mav_putvalue_src1= 0x86bde2c
#    mav_putvalue_src2= 0xc7235b6
#    mav_putvalue_src3= 0x74a8f5
#    EN_mav_putvalue = 1

#    mav_putvalue_instr = 0x8001033
#    mav_putvalue_src1 =0x1198497
#    mav_putvalue_src2 =0x3ad2c13
#    mav_putvalue_src3 =0x8c0eff4
#    EN_mav_putvalue = 1
#    mav_putvalue_instr = 0x4005033
#    mav_putvalue_src1 =2
#    mav_putvalue_src2 =0
#    mav_putvalue_src3 =0
#    EN_mav_putvalue = 1
#

    #mav_putvalue_instr = 0x28001033
    #mav_putvalue_src1 = 0xa1cca08
    #mav_putvalue_src2 = 0x155171d
    #mav_putvalue_src3 = 0xaeb38ba
    #EN_mav_putvalue = 1

   # mav_putvalue_instr = 0x68001033
   # mav_putvalue_src1 = 0xb3d3eff
   # mav_putvalue_src2 = 0x8db18b7
   # mav_putvalue_src3 = 0x5735e15
   # EN_mav_putvalue = 1

    yield InputTransaction(tb, mav_putvalue_instr, mav_putvalue_src1,
                        mav_putvalue_src2, mav_putvalue_src3,
                        EN_mav_putvalue)


@cocotb.coroutine
def clock_gen(signal):
    while True:
        signal <= 0
        yield Timer(1) # ps
        signal <= 1
        yield Timer(1) # ps

@cocotb.test()
def run_test(dut):
    cocotb.fork(clock_gen(dut.CLK))
    tb = Testbench(dut)
    dut.RST_N <= 1
    input_gen = random_input_gen(tb)
    # Issue first transaction immediately.
    #yield tb.input_drv.send(InputTransaction(tb))
    yield tb.input_drv.send(input_gen, False)

    for t in input_gen:
        yield tb.input_drv.send(t)

    yield tb.input_drv.send(InputTransaction(tb))
    tb.stop()
    yield RisingEdge(dut.CLK)


    #raise tb.scoreboard.result

    #factory = TestFactory(run_test)
    #factory.generate_tests()
    #print coverage report
    dut._log.info("Functional coverage details:")
    coverage_db.report_coverage(dut._log.info, bins=False)
    coverage_db.export_to_xml("coverage.xml")

    fh.close()

