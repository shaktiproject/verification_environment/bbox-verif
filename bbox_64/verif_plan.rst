
Bit manipulation 32 bit Verification Plan:
####################################


+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| Section | Title      | Description                                                                                                       |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.1     | ANDN       | and and not operations between two operands                                                                       |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.2     | ORN        | or and not operations between two operands                                                                        |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.3     | XNOR       | xor and not operations between two operands                                                                       |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.4     | SLO        | left shift operation by filling one's                                                                             |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.5     | SRO        | Right shift operation by filling one's                                                                            |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.6     | ROL        | Rotating Left operation between two operands                                                                      |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.7     | ROR        | Rotating Right operation between two operands                                                                     |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.8     | SH1ADD     | Address calculation instruction shift the operand left by one                                                     |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.9     | SH2ADD     | Address calculation instruction shift the operand left by two                                                     |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.10    | SH3ADD     | Address calculation instruction shift the operand left by three                                                   |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.11    | SBCLR      | Single bit operation for clear                                                                                    |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.12    | SBSET      | Single bit operation for set                                                                                      |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.13    | SBINV      | Single bit operation for invert                                                                                   |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.14    | SBEXT      | Single bit operation for extract                                                                                  |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.15    | GORC       | Generalized or function between two operands                                                                      |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.16    | GREV       | Generalized Reverse function by swapping each adjacent pair of 2**i bits                                          |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.17    | CMIX       | Conditional mix operation based on control word                                                                   |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.18    | CMOV       | Conditional move operation select the operand based on control word                                               |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.19    | FSL        | funnel Left shifting operations between the operands                                                              |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.20    | FSR        | funnel Right shifting operations between the operands                                                             |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.21    | CLZ        | Counts the no of 0 bits at the MSB end of the argument                                                            |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.22    | CTZ        | Counts the no of 0 bits at the LSB end of the argument                                                            |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.23    | PCNT       | Counts the no of 1 bits in the Register                                                                           |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.24    | SEXT.B     | Sign extending operation for byte                                                                                 |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.25    | SEXT.H     | Sign extending operation for Half                                                                                 |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.26    | CRC32.B    | Polynomial reduction of the operand shifted left by 8                                                             |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.27    | CRC32.H    | Polynomial reduction of the operand shifted left by 16                                                            |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.28    | CRC32.W    | Polynomial reduction of the operand shifted left by 32                                                            |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.29    | CRC32C.B   | Polynomial reduction of the operand shifted left by 8                                                             |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.30    | CRC32C.H   | Polynomial reduction of the operand shifted left by 16                                                            |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.31    | CRC32C.W   | Polynomial reduction of the operand shifted left by 32                                                            |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.32    | CLMUL      | Calculate Lower half of the Carry less product.                                                                   |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.33    | CLMULH     | Calculate upper half of the 2-XLEN Carry less product.                                                            |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.34    | CLMULR     | clmulh is equivalent to clmulr followed by a 1 bit right shift                                                    |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.35    | MIN        | Comparing operation between the operands                                                                          |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.36    | MAX        | Comparing operation between the operands                                                                          |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.37    | MINU       | Comparing operation between the operands                                                                          |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.38    | MAXU       | Comparing operation between the operands                                                                          |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.39    | BDEP       | Bit extract operation between two operands                                                                        |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.40    | BEXT       | Bit Deposit operation between two operands                                                                        |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.41    | PACK       | The pack instruction packs the Lower half of operand1 and upper half of the operand2                              |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.42    | PACKU      | The packu instruction packs the upper halves of operand1 and operand2 into Destination operand                    |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.43    | PACKH      | The packu instruction packs the lower halves of operand1 and operand2 into 16 LSB bits of the Destination operand |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.44    | SLOI       | Left shift operation by shifting ones                                                                             |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.45    | SROI       | Right shift operation by shifting ones                                                                            |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.46    | RORI       | Rotating Right operation between operand and immediate value                                                      |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.47    | SBCLRI     | Single bit operation for clear with immediate value                                                               |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.48    | SBSETI     | Single bit operation for set with immediate value                                                                 |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.49    | SBINVI     | Single bit operation for invert with immediate value                                                              |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.50    | SBEXTI     | Single bit operation for extract with immediate value                                                             |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.51    | SHFL       | Generalized shuffle swaps in MSB-LSB order based on control bits                                                  |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.52    | UNSHFL     | Generalized shuffle swaps in LSB-MSB order based on control bits                                                  |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.53    | SHFLI      | Generalized shuffle operation done with immediate value                                                           |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.54    | UNSHFLI    | Generalized shuffle operation done with immediate value                                                           |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.55    | GORCI      | Generalized or function between operand and immediate value                                                       |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.56    | GREVI      | Generalized Reverse function by swapping each adjacent pair of 2 power of i bits with immediate value             |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.57    | FSRI       | funnel Right shifting operations between operand and immediate value                                              |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.58    | BFP        | Bit-field place operation between the operands                                                                    |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
| 1.59    | error case | Instruction which is not coming under the instruction list the ouput[33] bit must be a zero                       |
+---------+------------+-------------------------------------------------------------------------------------------------------------------+
