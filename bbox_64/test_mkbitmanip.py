import random
import sys
import cocotb
import logging as log
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge
from cocotb.monitors import BusMonitor
from cocotb.drivers import BusDriver
from cocotb.binary import BinaryValue
from cocotb.regression import TestFactory
from cocotb.scoreboard import Scoreboard
from cocotb.result import TestFailure
from model_mkbitmanip import *
from cocotb.clock import Clock
from cocotb_coverage import crv
from cocotb_coverage import coverage

from model_mkbitmanip import *
from constants import *

class InputDriver(BusDriver):
    """Drives inputs to DUT."""
    _signals = ["mav_putvalue_instr", "mav_putvalue_src1",
                "mav_putvalue_src2", "mav_putvalue_src3", "EN_mav_putvalue"]

    def __init__(self, dut):
        BusDriver.__init__(self, dut, None, dut.CLK)


class InputTransaction(object):
    """Transactions to be sent by InputDriver"""
    def __init__(self, tb, mav_putvalue_instr=0, mav_putvalue_src1=0,
                    mav_putvalue_src2=0, mav_putvalue_src3=0,
                    EN_mav_putvalue=0):
        self.mav_putvalue_instr = BinaryValue(mav_putvalue_instr, tb.mav_putvalue_instr_bits, False)
        self.mav_putvalue_src1 = BinaryValue(mav_putvalue_src1, tb.mav_putvalue_src1_bits, False)
        self.mav_putvalue_src2 = BinaryValue(mav_putvalue_src2, tb.mav_putvalue_src2_bits, False)
        self.mav_putvalue_src3 = BinaryValue(mav_putvalue_src3, tb.mav_putvalue_src3_bits, False)
        self.EN_mav_putvalue = BinaryValue(EN_mav_putvalue, tb.EN_mav_putvalue_bits, False)

class InputMonitor(BusMonitor):
    """ Passive monitors of DUT."""
    _signals = ["mav_putvalue_instr", "mav_putvalue_src1",
                "mav_putvalue_src2","mav_putvalue_src3", "EN_mav_putvalue"]

    def __init__(self, dut, callback=None, event=None):
        BusMonitor.__init__(self, dut, None, dut.CLK, dut.RST_N,
                            callback=callback, event=event)
        self.name = "in"

    @coroutine
    def _monitor_recv(self):
        clkedge = RisingEdge(self.clock)

        while True:
            yield clkedge
            vec = ( self.bus.mav_putvalue_instr.value.integer,
                    self.bus.mav_putvalue_src1.value.integer,
                    self.bus.mav_putvalue_src2.value.integer,
                    self.bus.mav_putvalue_src3.value.integer,
                    self.bus.EN_mav_putvalue.value.integer)
            self._recv(vec)

class OutputTransaction(object):
    """Transaction to be expected / received by OutputMonitor."""

    def __init__(self, tb=None, mav_putvalue=0):
        """For expected transactions, value 'None' means don't care.
        tb must be an instance of the Testbench class."""
        if mav_putvalue is not None and isinstance(mav_putvalue, int):
            mav_putvalue = BinaryValue(mav_putvalue, tb.mav_putvalue_bits, False)

        self.value = (mav_putvalue)


class OutputMonitor(BusMonitor):
    """Observes outputs of DUT."""
    _signals = ["mav_putvalue","EN_mav_putvalue"]

    def __init__(self, dut, tb, callback=None, event=None):
        BusMonitor.__init__(self, dut, None, dut.CLK, dut.RST_N, callback=callback, event=event)
        self.name = "out"
        self.tb = tb

    @coroutine
    def _monitor_recv(self):
        clkedge = RisingEdge(self.clock)
        while True:
            yield clkedge
#            print(" dut mav_putvalue :",self.bus.mav_putvalue.value)
            recieved_mav_putvalue = self.bus.mav_putvalue.value
            self._recv(OutputTransaction(self.tb, recieved_mav_putvalue))

class Testbench(object):
    class MyScoreboard(Scoreboard):
        def compare(self, got, exp, log, **_):
            got1="".join(str(got.value))
            exp1="".join(str(exp.value))
            #log.info("\nExpected: {0!s}.\nReceived: {1!s}.".format(exp1, got1))
            if got1.strip() != exp1.strip():
                fh.write(" Expected: {0!s}. differ Received: {1!s}.\n".format(hex(int(exp1,2)), hex(int(got1,2))))
                log.warning("Expected: {0!s}. differ Received: {1!s}.".format(hex(int(exp1,2)), hex(int(got1,2))))
                #exit(1)
            else:
                fh.write('passed\n')

    def __init__(self, dut):
        self.dut = dut
        self.stopped = False
        self.mav_putvalue_instr_bits = 32
        self.mav_putvalue_src1_bits = 64
        self.mav_putvalue_src2_bits = 64
        self.mav_putvalue_src3_bits = 64
        self.EN_mav_putvalue_bits = 1
        self.mav_putvalue_bits = 65 
        self.input_mon = InputMonitor(dut, callback=self.model)

        init_val = OutputTransaction(self)

        self.input_drv = InputDriver(dut)
        self.output_mon = OutputMonitor(dut, self)

        # scoreboard on the outputs
        self.expected_output = []
        self.scoreboard = Testbench.MyScoreboard(dut)
        self.scoreboard.add_interface(self.output_mon, self.expected_output)	
        
   
   

    def model(self, transaction):
        """Model """
        mav_putvalue_instr, mav_putvalue_src1, mav_putvalue_src2, mav_putvalue_src3, EN_mav_putvalue = transaction
        #mav_putvalue = 0
        mav_putvalue = bitmanip(mav_putvalue_instr, mav_putvalue_src1, mav_putvalue_src2, mav_putvalue_src3, EN_mav_putvalue)
        fh.write("-- instr: {0} src1: {1} src2: {2} src3: {3} model: {4} ".format(hex(mav_putvalue_instr), hex(mav_putvalue_src1), hex(mav_putvalue_src2), hex(mav_putvalue_src3), hex(mav_putvalue)))
        self.expected_output.append( OutputTransaction(self, mav_putvalue) )

    def stop(self):
        """
        Stop generation of expected output transactions.
        One more clock cycle must be executed afterwards, so that, output of
        D-FF can be checked.
        """
        self.stopped = True


#--------------------------------------------------------------------------------------------------------------------------------
def random_input_gen(tb,n=10000):
  
    ins_list=[0x40007033,0x40006033,0x40004033,0x20001033,0x20005033,0x60001033,0x60005033,0x20002033,0x20004033,0x20006033,
              0x48001033,0x28001033,0x68001033,0x48005033,0x28005033,0x68005033,0x6001033,0x6005033,0x4001033,0x4005033, 
              0x60001013,0x60101013,0x60201013,0x60401013,0x60501013,0x61001013,0x61101013,0x61201013,0x61301013,0x61801013, 
              0x61901013,0x61A01013,0x61B01013,0xA001033,0xA003033,0xA002033,0xA004033,0xA005033,0xA006033,0xA007033,
              0x48006033,0x8006033,0x08004033,0x48004033,0x8007033,0xA00003B,0x4A00003B,0x800003B,0x4800003B,0x2000103B,
              0x2000503B,0x6000103B,0x6000503B,0x2000203B,0x2000403B,0x2000603B,0x4800103B,0x2800103B,0x6800103B,0x4800503B,
              0x2800503B,0x6800503B,0x400103B,0x400503B,0x6000101B,0x6010101B,0x6020101B,0xA00103B,0xA00303B,0xA00203B,
              0x4800603B,0x800603B,0x800403B,0x4800403B,0x20401013,0x20405013,0x60405013,0x48101013,0x28101013,0x68101013,
              0x48105013,0x10401B,0x810101B,0x2040101B,0x2040501B,0x6040501B,0x4810101B,0x2810101B,0x6810101B,0x8001033,
              0x8005033,0x8001013,0x8005013,0x800103B,0x800503B,0x60301013,0x28105013,0x2840501B,0x68405013,0x6810501B,
              0x4105013,0x410501B,0x48007033,0x4800703B,0x48003033,0x8003033] 
    class SimpleRandomized(crv.Randomized):
              """   Constrained Random Instruction generation  """
              def __init__(self, mav_putvalue_instr ):
                     crv.Randomized.__init__(self)
                     self.mav_putvalue_instr= mav_putvalue_instr
                     #defines random variables
                     # define mav_putvalue_instr as a random variable taking values from ins_list
                     self.add_rand("mav_putvalue_instr",ins_list)
                     c1 = lambda  mav_putvalue_instr:mav_putvalue_instr
                     #defining constraint for  instructions
                     self.add_constraint(c1)

    length = len(ins_list)
      # Iterating the index
      # same as 'for i in range(len(list))'
    for i in range(n):
            #for i in range(length):
            # create randomized object instance
            choose_instr = random.randint(0, length-1)
            x = SimpleRandomized(ins_list[choose_instr])
            # randomize object with additional contraint
            #performs a randomization for all random variables meeting all defined constraints
            x.randomize()
            mav_putvalue_instr = x.mav_putvalue_instr
            #mav_putvalue_instr = 0x4105013
            mav_putvalue_src1 = random.randint(0,0xFFFFFFFFFFFFFFF)
            mav_putvalue_src2 = random.randint(0,0xFFFFFFFFFFFFFFF)
            mav_putvalue_src3 = random.randint(0,0xFFFFFFFFFFFFFFF)
            EN_mav_putvalue = 1

            yield InputTransaction(tb, mav_putvalue_instr, mav_putvalue_src1,
                                mav_putvalue_src2, mav_putvalue_src3,
                                EN_mav_putvalue)




@cocotb.coroutine
def clock_gen(signal):
    while True:
        signal <= 0
        yield Timer(1) # ps
        signal <= 1
        yield Timer(1) # ps

@cocotb.test()
def run_test(dut):
    cocotb.fork(clock_gen(dut.CLK))
    tb = Testbench(dut)
    dut.RST_N <= 1
    input_gen = random_input_gen(tb)
    # Issue first transaction immediately.
    #yield tb.input_drv.send(InputTransaction(tb))
    yield tb.input_drv.send(input_gen, False)

    for t in input_gen:
        yield tb.input_drv.send(t)

    yield tb.input_drv.send(InputTransaction(tb))
    tb.stop()
    yield RisingEdge(dut.CLK)
    


    #raise tb.scoreboard.result

    #factory = TestFactory(run_test)
    #factory.generate_tests()
    #print coverage report
    dut._log.info("Functional coverage details:")
    coverage_db.report_coverage(dut._log.info, bins=False)
    coverage_db.export_to_xml("coverage.xml")

    fh.close()

