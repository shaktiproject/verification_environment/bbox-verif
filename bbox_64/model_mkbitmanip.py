from cocotb.binary import BinaryValue
from cocotb_coverage.coverage import * 
#import logging as log
import os
import sys
import operator
from cocotb.result import ReturnValue
from constants import *

bitmanip_Coverage = coverage_section (
         CoverPoint("bitmanip_model.instr", xf = lambda instr,a,b,c,en:instr , bins =[0x40007033,0x40006033,0x40004033,0x20001033,0x20005033,0x60001033,0x60005033,0x20002033,0x20004033,0x20006033]),
         CoverPoint("bitmanip_model.a", xf = lambda  instr,a,b,c,en:a , bins =[1]),
         CoverPoint("bitmanip_model.b", xf = lambda  instr,a,b,c,en:b , bins =[0]),
         CoverPoint("bitmanip_model.c", xf = lambda  instr,a,b,c,en:c , bins =[0] ),
     )


@bitmanip_Coverage
def bitmanip(mav_putvalue_instr, mav_putvalue_src1,mav_putvalue_src2, mav_putvalue_src3, EN_mav_putvalue): 
    instr_list=[
0x40007033,0x40006033,0x40004033,0x20001033,0x20005033,0x60001033,0x60005033,0x48001033,0x48005033,0x28001033,0x68001033,0x60001013,0x60101013,0x60201013,0x6000101B,0x6010101B,0x6020101B,0x08004033,0x48004033,0x8007033,0x20401013,0x20405013,0x60405013,0x20002033,0x20004033,0x20006033,0xA004033,0xA005033,0xA006033,0xA007033,0x68005033,0x48101013,0x48105013,0x28101013,0x68101013,0x68405013,0x28005033,0x28105013,0xA001033,0xA002033,0xA003033,0x6001033,0x6005033,0x4001033,0x4005033,0x400103B,0x400503B,0x4105013,0x410501B,0x8006033,0x48006033,0xA00003B,0x10401B,0x4A00003B,0x800003B,0x4800003B,0x810101B,0x2000103B,0x2000503B,0x6000103B,0x6000503B,0x4800103B,0x4800503B,0x2800103B,0x6800103B,0x2040101B,0x2040501B,0x6040501B,0x6800503B,0x4810101B,0x2810101B,0x6810101B,0x6810501B,0x2800503B,0x2840501B,0xA00103B,0xA00203B,0xA00303B,0x2000203B,0x2000403B,0x2000603B,0x800603B,0x4800603B,0x61001013,0x61101013,0x61201013,0x61301013,0x61801013,0x61901013,0x61A01013,0x61B01013,0x800403B,0x4800703B,0x8001033,0x8005033,0x8001013,0x8005013,0x800103B,0x800503B,0x48007033,0x4800703B,0x60401013,0x60501013,0x60301013,0x48003033,0x8003033
]
    instr=hex(mav_putvalue_instr)[2:]
    le=int(instr,16) #convert Hex  to int
    le=bin(le)[2:] #convert int to binary
    le=le.zfill(32)
    length=len(le)
    imm_value = le[length-25:length-20]
    imm_value=(int(str(imm_value),2))
    fsr_imm_value = le[length-27:length-20]
    fsr_imm_value=(int(str(fsr_imm_value),2))
    shamt_imm= imm_value & (63)
    shamt1= mav_putvalue_src2 & (63)
    mav_putvalue=0
    if(mav_putvalue_instr == 0x40007033):         #ANDN 1
        fh.write('------------------------------------------------------------------------------------------------------------------ANDN 1')
        mav_putvalue=mav_putvalue_src1 & (~mav_putvalue_src2)
        mav_putvalue=mav_putvalue & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x40006033):         #ORN 2
        fh.write('------------------------------------------------------------------------------------------------------------------ORN 2')
        mav_putvalue=mav_putvalue_src1 | (~mav_putvalue_src2)
        mav_putvalue=mav_putvalue & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x40004033):         #XNOR 3
        fh.write('------------------------------------------------------------------------------------------------------------------XNOR 3')
        mav_putvalue=mav_putvalue_src1 ^ (~mav_putvalue_src2)
        mav_putvalue=mav_putvalue & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x20001033):         #SLO  4
        fh.write('------------------------------------------------------------------------------------------------------------------SLO 4')
        if (shamt1):
            res=((mav_putvalue_src1)<< shamt1)
            min_i=0
            max_i=shamt1
            while (min_i<max_i):
                res=((1 << min_i) | res)
                min_i=min_i+1
                mav_putvalue=res & 0xffffffffffffffff
        else:
            mav_putvalue=0xFFFFFFFFFFFFFFFF
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x20005033):         #SRO  5 
        fh.write('------------------------------------------------------------------------------------------------------------------SRO 5')
        out=((mav_putvalue_src1)>> shamt1)
        res=out
        min_i=64-shamt1
        max_i=64
        while (min_i<max_i):
            res=((1 << min_i) | res)
            min_i=min_i+1
        mav_putvalue=res & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x60001033):         #ROL  6
        fh.write('------------------------------------------------------------------------------------------------------------------ROL 6')
        out=(mav_putvalue_src1 << shamt1) | (mav_putvalue_src1 >> ((64-shamt1) & (63)))
        mav_putvalue=out & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x60005033):         #ROR  7 
        fh.write('------------------------------------------------------------------------------------------------------------------ROR 7')
        out=(mav_putvalue_src1 >> shamt1) | (mav_putvalue_src1 << ((64-shamt1) & (63)))
        mav_putvalue=out & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x20002033):         #SH1ADD  8
        fh.write('------------------------------------------------------------------------------------------------------------------SH1ADD  8')
        out=(mav_putvalue_src1  << 1) +mav_putvalue_src2 
        mav_putvalue=out & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x20004033):         #SH2ADD  9
        fh.write('------------------------------------------------------------------------------------------------------------------SH2ADD  9')
        out=(mav_putvalue_src1  << 2) +mav_putvalue_src2
        mav_putvalue=out & 0xffffffffffffffff 
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x20006033):         #SH3ADD  10
        fh.write('------------------------------------------------------------------------------------------------------------------SH3ADD  10')
        out=(mav_putvalue_src1  << 3) +mav_putvalue_src2 
        mav_putvalue=out & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)
        
    elif(mav_putvalue_instr == 0x48001033):         #SBCLR   11
        fh.write('------------------------------------------------------------------------------------------------------------------SBCLR  11')
        out= mav_putvalue_src1 & (~(1<<shamt1))
        mav_putvalue=out & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x28001033):         #SBSET   12
        fh.write('------------------------------------------------------------------------------------------------------------------SBSET  12')
        out= mav_putvalue_src1 | (1<<shamt1)
        mav_putvalue=out & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x68001033):         #SBINV  13
        fh.write('------------------------------------------------------------------------------------------------------------------SBINV  13')
        out= mav_putvalue_src1  ^ (1<<shamt1)
        mav_putvalue=out & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x48005033):         #SBEXT  14
        fh.write('------------------------------------------------------------------------------------------------------------------SBEXT  14')
        out= 1 & (mav_putvalue_src1 >> shamt1)
        mav_putvalue=out & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    if(mav_putvalue_instr == 0x28005033):         #GORC 15 
        fh.write('------------------------------------------------------------------------------------------------------------------GORC  15')
        mav_putvalue=mav_putvalue_src1
        x=mav_putvalue_src1
        if (shamt1 & 1):
            x= x | ((x & 0x5555555555555555)<< 1) | (( x & 0xaaaaaaaaaaaaaaaa) >>1)
        if (shamt1 & 2):
            x= x | ((x & 0x3333333333333333)<< 2) | ((x & 0xcccccccccccccccc) >>2)
        if (shamt1 & 4):
            x= x | ((x & 0x0f0f0f0f0f0f0f0f)<< 4) | (( x & 0xf0f0f0f0f0f0f0f0) >>4)
        if (shamt1 & 8):
            x= x | ((x & 0x00ff00ff00ff00ff)<< 8) | ((x & 0xff00ff00ff00ff00) >>8)
        if (shamt1 & 16):
            x= x | ((x & 0x0000ffff0000ffff)<< 16) | ((x & 0xffff0000ffff0000) >>16)
        if (shamt1 & 32):
            x= x | ((x & 0x00000000ffffffff)<< 32) | ((x & 0xffffffff00000000) >>32)
        mav_putvalue=x & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    if (mav_putvalue_instr == 0x68005033):  #GREV  16 (check)
        fh.write('------------------------------------------------------------------------------------------------------------------GREV  16')
        x=mav_putvalue_src1
        mav_putvalue=mav_putvalue_src1
        if (shamt1 & 1):
            x= ((x & 0x5555555555555555)<< 1) | (( x & 0xaaaaaaaaaaaaaaaa) >>1)
            mav_putvalue=x & 0xffffffffffffffff
        if (shamt1 & 2):
            x= ((x & 0x3333333333333333)<< 2) | (( x & 0xcccccccccccccccc) >>2)
            mav_putvalue=x & 0xffffffffffffffff
        if (shamt1 & 4):
            x= ((x & 0x0f0f0f0f0f0f0f0f)<< 4) | (( x & 0xf0f0f0f0f0f0f0f0) >>4)
            mav_putvalue=x & 0xffffffffffffffff
        if (shamt1 & 8):
            x= ((x & 0x00ff00ff00ff00ff)<< 8) | (( x & 0xff00ff00ff00ff00) >>8)
            mav_putvalue=x & 0xffffffffffffffff
        if (shamt1 & 16):
            x= ((x & 0x0000ffff0000ffff)<< 16) | (( x & 0xffff0000ffff0000) >>16)
            mav_putvalue=x & 0xffffffffffffffff
        if (shamt1 & 32):
            x= ((x & 0x00000000ffffffff)<< 32) | (( x & 0xffffffff00000000) >>32)
            mav_putvalue=x & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)
        
    elif (mav_putvalue_instr == 0x6001033):  #CMIX  17  
        fh.write('------------------------------------------------------------------------------------------------------------------CMIX  17')
        out= (mav_putvalue_src1 & mav_putvalue_src2) |(mav_putvalue_src3 & (~mav_putvalue_src2))
        mav_putvalue=out & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x6005033):  #CMOV 18
        fh.write('------------------------------------------------------------------------------------------------------------------CMOV  18')
        if (mav_putvalue_src2):
            mav_putvalue=mav_putvalue_src1
        else:
            mav_putvalue=mav_putvalue_src3
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x4001033):  #FSL 19
        fh.write('------------------------------------------------------------------------------------------------------------------FSL  19')
        shamt12= mav_putvalue_src2 & (127)
        A= mav_putvalue_src1
        B= mav_putvalue_src3
        if(shamt12>=64):
            shamt12=shamt12-64
            A= mav_putvalue_src3
            B= mav_putvalue_src1
        if(shamt12):
            mav_putvalue= (A << shamt12) | (B >> (64-shamt12))
        else:
            mav_putvalue=A
        mav_putvalue=mav_putvalue & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x4005033):  #FSR  20(check)
        fh.write('------------------------------------------------------------------------------------------------------------------FSR   20')
        shamt12= mav_putvalue_src2 & (127)
        fh.write("----------shamt---------------",shamt12)
        A= mav_putvalue_src1
        B= mav_putvalue_src3
        if(shamt12>=64):
            shamt1=shamt12-64
            A= mav_putvalue_src3
            B= mav_putvalue_src1
        if(shamt12):
            mav_putvalue= (A >> shamt1) | (B << (64-shamt1) )
        else:
            mav_putvalue=A
        mav_putvalue=mav_putvalue & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x60001013):  #CLZ   21
        fh.write('------------------------------------------------------------------------------------------------------------------CLZ  21')
        x=bin(mav_putvalue_src1)[2:]
        x=x.zfill(64)
        mav_putvalue = len(x.split('1', 1)[0])
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x60101013):  #CTZ    22
        fh.write('------------------------------------------------------------------------------------------------------------------CTZ   22')
        x=bin(mav_putvalue_src1)[2:]
        x=x.zfill(64)
        m = str(x)
        mav_putvalue = len(m)-len(m.rstrip('0'))
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x60201013):  #PCNT   23
        fh.write('------------------------------------------------------------------------------------------------------------------PCNT   23')
        binary = bin(mav_putvalue_src1)  
        setBits = [ones for ones in binary[2:] if ones=='1'] 
        mav_putvalue= len(setBits) 
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x60401013):  #SEXT.B  24 
        fh.write('------------------------------------------------------------------------------------------------------------------SEXT.B 24 ')
        le=mav_putvalue_src1
        lex=bin(le)[2:] 
        lex=lex.zfill(64)
        imm_value = lex[56]
        min_i=8
        max_i=64
        while (min_i<max_i):
            if(imm_value=='1'):
                le=((1 << min_i) | le)
                min_i=min_i+1
            else:
                le=((1 << min_i) | le)
                min_i=min_i+1
                le=le & 0x00000000000000ff
        mav_putvalue=le & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x60501013):  #SEXT.H  25  
        fh.write('------------------------------------------------------------------------------------------------------------------SEXT.B 24 ')

        le=mav_putvalue_src1
        lex=bin(le)[2:] 
        lex=lex.zfill(64)
        imm_value = lex[48]
        min_i=16
        max_i=64
        while (min_i<max_i):
            if(imm_value=='1'):
                le=((1 << min_i) | le)
                min_i=min_i+1
            else:
                le=((1 << min_i) | le)
                min_i=min_i+1
                le=le & 0x000000000000ffff
        mav_putvalue=le & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x61001013):  #CRC32.B 26
        fh.write('------------------------------------------------------------------------------------------------------------------CRC32.B  26')
        i=0
        x = mav_putvalue_src1
        while i<8:
            x = (x  >> 1) ^ (0xedb88320 & ~((x &1)-1));
            i+=1
        mav_putvalue=x
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x61101013):  #CRC32.H  27
        fh.write('------------------------------------------------------------------------------------------------------------------CRC32.H  27')
        i=0
        x = mav_putvalue_src1
        while i<16:
            x = (x  >> 1) ^ (0xedb88320 & ~((x &1)-1));
            i+=1
        mav_putvalue=x
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x61201013):  #CRC32.W  28
        fh.write('-----------------------------------------------------------------------------------------------------------------CRC32.W  28 ')
        i=0
        x = mav_putvalue_src1
        while i<32:
            x = (x  >> 1) ^ (0xedb88320 & ~((x &1)-1));
            i+=1
        mav_putvalue=x
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x61301013):  #CRC32.D  29
        fh.write('------------------------------------------------------------------------------------------------------------------CRC32.D  29 ')
        i=0
        x = mav_putvalue_src1
        while i<64:
            x = (x  >> 1) ^ (0xedb88320 & ~((x &1)-1));
            i+=1
        mav_putvalue=x
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x61801013):  #CRC32C.B 30
        fh.write('-----------------------------------------------------------------------------------------------------------------CRC32C.B  30')
        i=0
        x = mav_putvalue_src1
        while i<8:
            x = (x  >> 1) ^ (0x82F63B78 & ~((x &1)-1));
            i+=1
        mav_putvalue=x
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x61901013):  #CRC32C.H  31
        fh.write('-----------------------------------------------------------------------------------------------------------------CRC32C.H  31')
        i=0
        x = mav_putvalue_src1
        while i<16:
            x = (x  >> 1) ^ (0x82F63B78 & ~((x &1)-1));
            i+=1
        mav_putvalue=x
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x61A01013):  #CRC32C.W  32
        fh.write('-----------------------------------------------------------------------------------------------------------------CRC32C.W  32 ')
        i=0
        x = mav_putvalue_src1
        while i<32:
            x = (x  >> 1) ^ (0x82F63B78 & ~((x &1)-1));
            i+=1
        mav_putvalue=x
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x61B01013):  #CRC32C.D  33
        fh.write('------------------------------------------------------------------------------------------------------------------CRC32C.D  33')
        i=0
        x = mav_putvalue_src1
        while i<64:
            x = (x  >> 1) ^ (0x82F63B78 & ~((x &1)-1));
            i+=1
        mav_putvalue=x
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0xA001033):    #CLMUL  34
        fh.write('------------------------------------------------------------------------------------------------------------------CLMUL   34')
        x=0
        i=0
        while i<64:
            if ((mav_putvalue_src2 >> i) & 1):
                x =x ^ mav_putvalue_src1 << i
            i=i+1
        mav_putvalue=x & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0xA003033):    #CLMULH  35
        fh.write('------------------------------------------------------------------------------------------------------------------CLMULH 35 ')
        x=0
        i=1
        while i<64:
            if ((mav_putvalue_src2 >> i) & 1):
                x =x ^ mav_putvalue_src1 >> (64-i)
            i=i+1
        mav_putvalue=x & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0xA002033):    #CLMULR  36
        fh.write('------------------------------------------------------------------------------------------------------------------CLMULR 36 ')
        x=0
        i=0
        while i<64:
            if ((mav_putvalue_src2 >> i) & 1):
                x =x ^ mav_putvalue_src1 >> (64-i-1)
            i=i+1
        mav_putvalue=x & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0xA004033):    #MIN  37
        fh.write('------------------------------------------------------------------------------------------------------------------MIN 37 ')
        le1=mav_putvalue_src1
        lex=bin(le1)[2:] 
        lex=lex.zfill(64)
        imm_value1 = lex[0]
        le2=mav_putvalue_src2
        ex=bin(le2)[2:] 
        ex=ex.zfill(64)
        imm_value2 = ex[0]
        if(imm_value1=='1') and (imm_value2=='0'):
            return ((mav_putvalue_src1 <<1)|1)
        if(imm_value1=='0') and (imm_value2=='1'):
            return ((mav_putvalue_src2 <<1)|1)
        if((imm_value1=='0') and (imm_value2=='0')) or ((imm_value1=='1') and (imm_value2=='1')) :
            if (mav_putvalue_src1 < mav_putvalue_src2):
                return ((mav_putvalue_src1 <<1)|1)
            else:
                return ((mav_putvalue_src2 <<1)|1)

    elif (mav_putvalue_instr == 0xA005033):    #MAX 38
        fh.write('------------------------------------------------------------------------------------------------------------------MAX 38 ')
        le1=mav_putvalue_src1
        lex=bin(le1)[2:] 
        lex=lex.zfill(64)
        imm_value1 = lex[0]
        le2=mav_putvalue_src2
        ex=bin(le2)[2:] 
        ex=ex.zfill(64)
        imm_value2 = ex[0]
        if(imm_value1=='1') and (imm_value2=='0'):
            return ((mav_putvalue_src2 <<1)|1)
        if(imm_value1=='0') and (imm_value2=='1'):
            return ((mav_putvalue_src1 <<1)|1)
        if((imm_value1=='0') and (imm_value2=='0')) or ((imm_value1=='1') and (imm_value2=='1'))  :
            if (mav_putvalue_src1 > mav_putvalue_src2):
                return ((mav_putvalue_src1 <<1)|1)
            else:
                return ((mav_putvalue_src2 <<1)|1)

    elif (mav_putvalue_instr == 0xA006033):    #MINU  39
        fh.write('------------------------------------------------------------------------------------------------------------------MINU 39 ')
        if (mav_putvalue_src1 <  mav_putvalue_src2):
            mav_putvalue=mav_putvalue_src1
        else:
             mav_putvalue=mav_putvalue_src2
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0xA007033):    #MAXU 40
        fh.write('------------------------------------------------------------------------------------------------------------------MAXU 40 ')
        if (mav_putvalue_src1 >  mav_putvalue_src2):
            mav_putvalue=mav_putvalue_src1
        else:
             mav_putvalue=mav_putvalue_src2
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x48006033):    #BDEP 41 
        fh.write('------------------------------------------------------------------------------------------------------------------BDEP 41 ')
        r=0
        j=0
        for i in range(64):
            if ((mav_putvalue_src2 >> i) & 1):
                if ((mav_putvalue_src1 >> j) & 1):
                    r |= 1 << i  
                j=j+1          
        mav_putvalue=r
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x8006033):    #BEXT 42 
        fh.write('------------------------------------------------------------------------------------------------------------------BEXT 42 ')
        r=0
        j=0
        for i in range(64):
                if ((mav_putvalue_src2 >> i) & 1):
                    if ((mav_putvalue_src1 >> i) & 1):
                        r |= 1 << j 
                    j=j+1           
        mav_putvalue=r
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x08004033):    #PACK 43  
        fh.write('------------------------------------------------------------------------------------------------------------------PACK 43 ')
        lower = (mav_putvalue_src1 << 32) >> 32
        lower=lower & 0x00000000ffffffff
        upper =mav_putvalue_src2 << 32
        mav_putvalue=lower | upper
        mav_putvalue=mav_putvalue & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x48004033):    #PACKU 44 
        fh.write('------------------------------------------------------------------------------------------------------------------PACKU  44 ')
        lower = (mav_putvalue_src1 >> 32) 
        upper =mav_putvalue_src2 >> 32 << 32
        mav_putvalue=lower | upper
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x8007033):    #PACKH 45 
        fh.write('------------------------------------------------------------------------------------------------------------------PACKH 45 ')
        lower = mav_putvalue_src1& 255
        upper = (mav_putvalue_src2 & 255) << 8
        mav_putvalue=lower | upper
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0xA00003B):    #ADDWU 46
        fh.write('------------------------------------------------------------------------------------------------------------------ADDWU 46 ')
        mav_putvalue= mav_putvalue_src1 + mav_putvalue_src2
        mav_putvalue=mav_putvalue & 0x00000000ffffffff
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x4A00003B):    #SUBWU 47
        fh.write('------------------------------------------------------------------------------------------------------------------SUBWU 47 ')
        mav_putvalue= mav_putvalue_src1 - mav_putvalue_src2
        mav_putvalue=mav_putvalue & 0x00000000ffffffff
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x800003B):    #ADDU.W 48 
        fh.write('------------------------------------------------------------------------------------------------------------------ADDU.W 48 ')
        mav_putvalue_src2=mav_putvalue_src2 & 0x00000000ffffffff
        mav_putvalue= mav_putvalue_src1 + mav_putvalue_src2
        mav_putvalue=mav_putvalue & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x4800003B):    #SUBU.W 49
        fh.write('------------------------------------------------------------------------------------------------------------------SUBU.W 49 ')
        mav_putvalue_src2=mav_putvalue_src2 & 0x00000000ffffffff
        mav_putvalue= mav_putvalue_src1 - mav_putvalue_src2
        mav_putvalue=mav_putvalue & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    def sign_exe(mav_putvalue):
        le=mav_putvalue
        lex=bin(le)[2:] 
        lex=lex.zfill(64)
        imm_value = lex[32]
        min_i=32
        max_i=64  
        if(imm_value=='0'):
            le = mav_putvalue & 0x00000000FFFFFFFF
            return le
        if(imm_value=='1'):
            while (min_i<max_i):
                le=((1 << min_i) | le)
                min_i=min_i+1
            return le

    if(mav_putvalue_instr == 0x2000103B):         #SLOW  50
        fh.write('------------------------------------------------------------------------------------------------------------------SLOW 50 ')
        src1= mav_putvalue_src1 & 0x00000000FFFFFFFF
        min_i=0
        if (shamt1>=32):
            max_i=shamt1-32
            res=((src1)<< (shamt1-32))
        else:
            max_i=shamt1
            res=((src1)<< shamt1)
        while (min_i<max_i):
            res=((1 << min_i) | res)
            min_i=min_i+1
        mav_putvalue=sign_exe(res)
        return ((mav_putvalue<<1)|1)

    if(mav_putvalue_instr == 0x2000503B):         #SROW  51  
        fh.write('------------------------------------------------------------------------------------------------------------------SROW 51 ')
        src1= mav_putvalue_src1 & 0x00000000FFFFFFFF        
        max_i=64
        fh.write("----------shmat------",shamt1)
        if (shamt1<32):
            res=((src1)>> shamt1)
            min_i=32-shamt1
            while (min_i<max_i):
                res=((1 << min_i) | res)
                min_i=min_i+1
        else:
            res=(src1>> (shamt1-32))
            min_i=64-shamt1
            while (min_i<max_i):
                res=((1 << min_i) | res)
                min_i=min_i+1

        mav_putvalue=sign_exe(res)
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x6000103B):         #ROLW  52 
        fh.write('------------------------------------------------------------------------------------------------------------------ROLW 52 ')
        src1= mav_putvalue_src1 & 0x00000000FFFFFFFF 
        if (shamt1<32):               
            out=(src1 << shamt1) | (src1 >> ((32-shamt1) & (31)))  
        else:
            shamt=shamt1-32
            out=(src1 << shamt) | (src1 >> ((32-shamt) & (31)))  
        mav_putvalue=out & 0x00000000ffffffff
        mav_putvalue=sign_exe(mav_putvalue)
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x6000503B):         #RORW  53 
        fh.write('------------------------------------------------------------------------------------------------------------------RROW 53 ')
        src1= mav_putvalue_src1 & 0x00000000FFFFFFFF 
        if (shamt1<32):               
            out=(src1 >> shamt1) | (src1 << ((32-shamt1) & (31)))  
        else:
            shamt=shamt1-32
            out=(src1 >> shamt) | (src1 << ((32-shamt) & (31)))  
        mav_putvalue=out & 0x00000000ffffffff
        mav_putvalue=sign_exe(mav_putvalue)
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x2000203B):         #SH1ADDU.W  54
        fh.write('------------------------------------------------------------------------------------------------------------------SH1ADDU.W 54 ')
        src1= mav_putvalue_src1 & 0x00000000FFFFFFFF
        mav_putvalue=(src1  << 1) +mav_putvalue_src2 
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x2000403B):         #SH2ADDU.W  55
        fh.write('------------------------------------------------------------------------------------------------------------------SH2ADDU.W 55 ')
        src1= mav_putvalue_src1 & 0x00000000FFFFFFFF
        mav_putvalue=(src1  << 2) +mav_putvalue_src2 
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x2000603B):         #SH3ADDU.W  56
        fh.write('------------------------------------------------------------------------------------------------------------------SH3ADDU.W 56 ')
        src1= mav_putvalue_src1 & 0x00000000FFFFFFFF
        mav_putvalue=(src1  << 3) +mav_putvalue_src2 
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x4800103B):         #SBCLRW   57 
        fh.write('------------------------------------------------------------------------------------------------------------------SBCLRW 57 ')
        src1= mav_putvalue_src1 & 0x00000000FFFFFFFF
        if shamt1>=32:
            shamt1=shamt1-32
            out= src1 & (~(1<<shamt1))
        else:
            out= src1 & (~(1<<shamt1))            
        mav_putvalue=sign_exe(out)
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x2800103B):         #SBSETW   58 
        fh.write('------------------------------------------------------------------------------------------------------------------SBSETW 58 ')
        src1= mav_putvalue_src1 & 0x00000000FFFFFFFF
        if shamt1>=32:
            shamt1=shamt1-32
            out= src1 | (1<<shamt1)
        else:
            out= src1 | (1<<shamt1)
        mav_putvalue=sign_exe(out)
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x6800103B):         #SBINVW  59 (ch)
        fh.write('------------------------------------------------------------------------------------------------------------------SBINVW 59 ')
        src1= mav_putvalue_src1 & 0x00000000FFFFFFFF
        if shamt1>=32:
            shamt1=shamt1-32
            out= src1  ^ (1<<shamt1)
        else:
            out= src1  ^ (1<<shamt1)
        mav_putvalue=sign_exe(out)
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x4800503B):         #SBEXTW  60 
        fh.write('------------------------------------------------------------------------------------------------------------------SBEXTW 60 ')
        src1= mav_putvalue_src1 & 0x00000000FFFFFFFF
        if shamt1>=32:
            shamt1=shamt1-32
            out= 1 & (src1 >> shamt1)
        else:
            out= 1 & (src1 >> shamt1)      
        mav_putvalue=sign_exe(out)
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x2800503B):         #GORCW   61 
        fh.write('------------------------------------------------------------------------------------------------------------------GORCW 61 ')
        mav_putvalue=mav_putvalue_src1
        x= mav_putvalue_src1 & 0x00000000FFFFFFFF
        if (shamt1 & 1):
            x= x | ((x & 0x5555555555555555)<< 1) | ((x & 0xaaaaaaaaaaaaaaaa) >>1)
        if (shamt1 & 2):
            x= x | ((x & 0x3333333333333333)<< 2) | (( x & 0xcccccccccccccccc) >>2)
        if (shamt1 & 4):
            x= x | ((x & 0x0f0f0f0f0f0f0f0f)<< 4) | (( x & 0xf0f0f0f0f0f0f0f0) >>4)
        if (shamt1 & 8):
            x= x | ((x & 0x00ff00ff00ff00ff)<< 8) | ((x & 0xff00ff00ff00ff00) >>8)
        if (shamt1 & 16):
            x= x | ((x & 0x0000ffff0000ffff)<< 16) | (( x & 0xffff0000ffff0000) >>16)
        if (shamt1 & 32):
            x= x | ((x & 0x00000000ffffffff)<< 32) | ((x & 0xffffffff00000000) >>32)
        mav_putvalue=x & 0x00000000FFFFFFFF
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x6800503B):  #GREVW  62 
        fh.write('------------------------------------------------------------------------------------------------------------------GREVW 62 ')
        x= mav_putvalue_src1 & 0x00000000FFFFFFFF
        if (shamt1 & 1):
            x= ((x & 0x5555555555555555)<< 1) | ((x & 0xaaaaaaaaaaaaaaaa) >>1)
        if (shamt1 & 2):
            x= ((x & 0x3333333333333333)<< 2) | (( x & 0xcccccccccccccccc) >>2)
        if (shamt1 & 4):
            x= ((x & 0x0f0f0f0f0f0f0f0f)<< 4) | (( x & 0xf0f0f0f0f0f0f0f0) >>4)
        if (shamt1 & 8):
            x= ((x & 0x00ff00ff00ff00ff)<< 8) | (( x & 0xff00ff00ff00ff00) >>8)
        if (shamt1 & 16):
            x= ((x & 0x0000ffff0000ffff)<< 16) | ((x & 0xffff0000ffff0000) >>16)
        if (shamt1 & 32):
            x= ((x & 0x00000000ffffffff)<< 32) | (( x & 0xffffffff00000000) >>32)
            x=x>>32
        mav_putvalue=x 
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x400103B):  #FSLW 63  
        fh.write('------------------------------------------------------------------------------------------------------------------FSLW 63 ') 
        src1=mav_putvalue_src1 & 0x00000000FFFFFFFF
        src2=mav_putvalue_src2 & 0x00000000FFFFFFFF
        src3=mav_putvalue_src3 & 0x00000000FFFFFFFF
        shamt1= mav_putvalue_src2 & (63)
        A= src1 
        B= src3                 
        if(shamt1>=32):
            shamt1=shamt1-32
            A= src3  
            B= src1 
        if(shamt1):
            mav_putvalue= (A << shamt1) | (B >> (32-shamt1))
        else:
            mav_putvalue=A
        mav_putvalue=sign_exe(mav_putvalue)
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x400503B):  #FSRW  64   
        fh.write('------------------------------------------------------------------------------------------------------------------FSRW 64 ')
        shamt1= mav_putvalue_src2 & (63)
        src1=mav_putvalue_src1 & 0x00000000FFFFFFFF
        src2=mav_putvalue_src2 & 0x00000000FFFFFFFF
        src3=mav_putvalue_src3 & 0x00000000FFFFFFFF
        A= src1
        B= src3
        if(shamt1>=32):
            shamt1=shamt1-32
            A= src3
            B= src1
        if(shamt1):
            mav_putvalue= (A >> shamt1) | (B << (32-shamt1))
        else:
            mav_putvalue=A
        mav_putvalue=sign_exe(mav_putvalue)
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr ==  0x6000101B):  #CLZW   65
        fh.write('------------------------------------------------------------------------------------------------------------------CLZW 65 ')
        src1= mav_putvalue_src1 & 0x00000000FFFFFFFF
        x=bin(src1)[2:]
        x=x.zfill(32)
        mav_putvalue = len(x.split('1', 1)[0])
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x6010101B):  #CTZW    66 
        fh.write('------------------------------------------------------------------------------------------------------------------CTZW 66 ')
        src1= mav_putvalue_src1 & 0x00000000FFFFFFFF
        x=bin(src1)[2:]
        x=x.zfill(32)
        m = str(x)
        mav_putvalue = len(m)-len(m.rstrip('0'))
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x6020101B):  #PCNTW   67
        fh.write('------------------------------------------------------------------------------------------------------------------PCNTW 67 ')
        src1= mav_putvalue_src1 & 0x00000000FFFFFFFF
        binary = bin(src1)  
        setBits = [ones for ones in binary[2:] if ones=='1'] 
        mav_putvalue= len(setBits) 
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0xA00103B):    #CLMULW  68 
        fh.write('------------------------------------------------------------------------------------------------------------------CLMULW 68 ')
        src1= mav_putvalue_src1 & 0x00000000FFFFFFFF
        src2= mav_putvalue_src2 & 0x00000000FFFFFFFF        
        x=0
        i=0
        while i<64:
            if ((src2 >> i) & 1):
                x =x ^ src1 << i
            i=i+1
        mav_putvalue=sign_exe(x)
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0xA00303B):    #CLMULHW  69  
        fh.write('------------------------------------------------------------------------------------------------------------------CLMULHW 69 ')
        src1= mav_putvalue_src1 & 0x00000000FFFFFFFF
        src2= mav_putvalue_src2 & 0x00000000FFFFFFFF        
        x=0
        i=1
        while i<64:
            if ((src2 >> i) & 1):
                x =x ^ src1 >> (32-i)
            i=i+1
        mav_putvalue=x & 0x00000000ffffffff
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0xA00203B):    #CLMULRW  70 
        fh.write('------------------------------------------------------------------------------------------------------------------CLMULRW 70 ')
        src1= mav_putvalue_src1 & 0x00000000FFFFFFFF
        src2= mav_putvalue_src2 & 0x00000000FFFFFFFF        
        x=0
        i=0
        while i<64:
            if ((src2 >> i) & 1):
                x =x ^ src1 >> (32-i-1)
            i=i+1
        mav_putvalue=sign_exe(x)
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x4800603B):    #BDEPW 71
        fh.write('------------------------------------------------------------------------------------------------------------------BDEPW 71 ')
        r=0
        j=0
        for i in range(64):
            if ((mav_putvalue_src2 >> i) & 1):
                if ((mav_putvalue_src1 >> j) & 1):
                    r |= 1 << i  
                j=j+1          
        mav_putvalue=sign_exe(r)
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x800603B):    #BEXTW 72 
        fh.write('------------------------------------------------------------------------------------------------------------------BEXTW 72 ')
        src1= mav_putvalue_src1 & 0x00000000FFFFFFFF
        src2= mav_putvalue_src2 & 0x00000000FFFFFFFF 
        r=0
        j=0
        for i in range(32):
                if ((src2 >> i) & 1):
                    if ((src1 >> i) & 1):
                        r |= 1 << j 
                    j=j+1           
        mav_putvalue=r & 0x0000000ffffffff
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x800403B):    #PACKW 73 
        fh.write('------------------------------------------------------------------------------------------------------------------PACKW 73 ')
        lower = (mav_putvalue_src1 << 32) >> 32
        lower=lower & 0x000000000000ffff
        upper =mav_putvalue_src2 << 16
        mav_putvalue=lower | upper
        out= mav_putvalue & 0x00000000FFFFFFFF
        mav_putvalue=sign_exe(out)
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x4800403B):    #PACKUW 74 
        fh.write('------------------------------------------------------------------------------------------------------------------PACKUW 74 ')  
        src1= mav_putvalue_src1 & 0x00000000FFFFFFFF
        src2= mav_putvalue_src2 & 0x00000000FFFFFFFF    
        lower = (src1 >> 16)
        upper =src2 >> 16 << 16
        mav_putvalue=lower | upper
        mav_putvalue=sign_exe(mav_putvalue)
        return ((mav_putvalue<<1)|1)

#-----------------immediate instructions-------------------------------------

    elif(mav_putvalue_instr == 0x20401013):         #SLOI  75
        fh.write('------------------------------------------------------------------------------------------------------------------SLOI 75 ')
        out=((mav_putvalue_src1)<< shamt_imm)
        res=out
        min_i=0
        max_i=shamt_imm
        while (min_i<max_i):
            res=((1 << min_i) | res)
            min_i=min_i+1
        mav_putvalue=res & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x20405013):         #SROI 76  
        fh.write('------------------------------------------------------------------------------------------------------------------SROI 76 ') 
        out=((mav_putvalue_src1)>> shamt_imm)
        res=out
        min_i=64-shamt_imm
        max_i=64
        while (min_i<max_i):
            res=((1 << min_i) | res)
            min_i=min_i+1
        mav_putvalue=res & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x60405013):         #RORI  77 
        fh.write('------------------------------------------------------------------------------------------------------------------RROI 77 ') 
        out=(mav_putvalue_src1 >> shamt_imm) | (mav_putvalue_src1 << ((64-shamt_imm) & (63)))
        mav_putvalue=out & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x48101013):         #SBCLRI   78
        fh.write('------------------------------------------------------------------------------------------------------------------SBCLRI 78 ') 
        out= mav_putvalue_src1 & (~(1<<shamt_imm))
        mav_putvalue=out & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x28101013):         #SBSETI   79
        fh.write('------------------------------------------------------------------------------------------------------------------SBSETI 79 ') 
        out= mav_putvalue_src1 | (1<<shamt_imm)
        mav_putvalue=out & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x68101013):         #SBINVI  80
        fh.write('------------------------------------------------------------------------------------------------------------------BINVI 80 ') 
        out= mav_putvalue_src1  ^ (1<<shamt_imm)
        mav_putvalue=out & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x48105013):         #SBEXTI  81
        fh.write('------------------------------------------------------------------------------------------------------------------SBEXTI 81 ') 
        out= 1 & (mav_putvalue_src1 >> shamt_imm)
        mav_putvalue=out & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x10401B):    #ADDIWU   82
        fh.write('------------------------------------------------------------------------------------------------------------------ADDIWU 82 ') 
        imm_val = le[length-32:length-20]
        imm_val=(int(str(imm_val),2))
        mav_putvalue= mav_putvalue_src1 + imm_val
        mav_putvalue=mav_putvalue & 0x00000000ffffffff
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x810101B):    #SLLIU.W   83       
        fh.write('------------------------------------------------------------------------------------------------------------------SLLIW 83 ') 
        mav_putvalue_src1=mav_putvalue_src1 & 0x00000000ffffffff
        mav_putvalue= mav_putvalue_src1 << shamt_imm
        mav_putvalue=mav_putvalue & 0x0000000fffffffff
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x2040101B):         #SLOIW  84  
        fh.write('------------------------------------------------------------------------------------------------------------------SLOIW 84 ') 
        out=((mav_putvalue_src1)<< shamt_imm)
        res=out
        min_i=0
        max_i=shamt_imm
        while (min_i<max_i):
            res=((1 << min_i) | res)
            min_i=min_i+1
        res=res & 0x00000000FFFFFFFF
        mav_putvalue=sign_exe(res)
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x2040501B):         #SROIW  85 
        fh.write('------------------------------------------------------------------------------------------------------------------SROIW 85 ') 
        src1= mav_putvalue_src1 & 0x00000000FFFFFFFF        
        max_i=64
        if (shamt_imm<=32):
            res=((src1)>> shamt_imm)
            min_i=32-shamt_imm
            while (min_i<max_i):
                res=((1 << min_i) | res)
                min_i=min_i+1
        else:
            res=(src1>> (shamt_imm-32))
            min_i=64-shamt1
            while (min_i<max_i):
                res=((1 << min_i) | res)
                min_i=min_i+1
        mav_putvalue=res & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x6040501B):         #RORIW  86  
        fh.write('------------------------------------------------------------------------------------------------------------------RORIW 86 ') 
        src1= mav_putvalue_src1 & 0x00000000FFFFFFFF 
        out=(src1 >> shamt_imm) | (src1 << ((32-shamt_imm) & (31)))    
        mav_putvalue=out & 0x00000000ffffffff
        mav_putvalue=sign_exe(mav_putvalue)
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x4810101B):         #SBCLRIW   87 
        fh.write('------------------------------------------------------------------------------------------------------------------SBCLRIW 87 ') 
        out= mav_putvalue_src1 & (~(1<<shamt_imm))
        mav_putvalue=sign_exe(out)
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x2810101B):         #SBSETIW   88  
        fh.write('------------------------------------------------------------------------------------------------------------------SBSETIW 88 ') 
        out= mav_putvalue_src1 | (1<<shamt_imm)
        mav_putvalue=sign_exe(out)
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x6810101B):         #SBINVIW  89
        fh.write('------------------------------------------------------------------------------------------------------------------SBINVIW 89 ') 
        out= mav_putvalue_src1  ^ (1<<shamt_imm)
        mav_putvalue=sign_exe(out)
        return ((mav_putvalue<<1)|1)

    def suffle64(mav_putvalue_src1,maskl,maskr,n):
        x = mav_putvalue_src1 & ~(maskl | maskr)
        x =x | ((mav_putvalue_src1 << n) & maskl) | ((mav_putvalue_src1 >> n) & maskr)
        return x
    def shfl_64(mav_putvalue_src1,mav_putvalue_src2):
        x=mav_putvalue_src1
        mav_putvalue=mav_putvalue_src1
        shamt= mav_putvalue_src2 & (31)
        if(shamt & 16):
            x=suffle64(x, 0x0000ffff00000000,0x00000000ffff0000,16)
        if(shamt & 8):
            x=suffle64(x, 0x00ff000000ff0000,0x0000ff000000ff00,8)
        if(shamt & 4):
            x=suffle64(x, 0x0f000f000f000f00,0x00f000f000f000f0,4)
        if(shamt & 2):
            x=suffle64(x, 0x3030303030303030,0x0c0c0c0c0c0c0c0c,2)
        if(shamt & 1):
            x=suffle64(x, 0x4444444444444444,0x2222222222222222,1)
        return x
    def bmatflip(mav_putvalue_src1):
        x= mav_putvalue_src1 
        x = shfl_64(x, 31)
        x = shfl_64(x, 31)
        x = shfl_64(x, 31)
        return x

        return mav_putvalue
    if(mav_putvalue_instr == 0x8001033):         #SHFL  90 
        fh.write('------------------------------------------------------------------------------------------------------------------SHFL 90 ') 
        x=mav_putvalue_src1
        mav_putvalue=mav_putvalue_src1
        shamt= mav_putvalue_src2 & (31)
        if(shamt & 16):
            x=suffle64(x, 0x0000ffff00000000,0x00000000ffff0000,16)
        if(shamt & 8):
            x=suffle64(x, 0x00ff000000ff0000,0x0000ff000000ff00,8)
        if(shamt & 4):
            x=suffle64(x, 0x0f000f000f000f00,0x00f000f000f000f0,4)
        if(shamt & 2):
            x=suffle64(x, 0x3030303030303030,0x0c0c0c0c0c0c0c0c,2)
        if(shamt & 1):
            x=suffle64(x, 0x4444444444444444,0x2222222222222222,1)       
        mav_putvalue=x & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x8005033):         #UNSHFL  91  
        fh.write('------------------------------------------------------------------------------------------------------------------UNSHFL 91 ')
        x=mav_putvalue_src1
        mav_putvalue=mav_putvalue_src1 
        shamt= mav_putvalue_src2 & (31)
        if(shamt & 1):
            x=suffle64(x, 0x4444444444444444,0x2222222222222222,1)
        if(shamt & 2):
            x=suffle64(x, 0x3030303030303030,0x0c0c0c0c0c0c0c0c,2)
        if(shamt & 4):
            x=suffle64(x, 0x0f000f000f000f00,0x00f000f000f000f0,4)
        if(shamt & 8):
            x=suffle64(x, 0x00ff000000ff0000,0x0000ff000000ff00,8)
        if(shamt & 16):
            x=suffle64(x, 0x0000ffff00000000,0x00000000ffff0000,16)
        mav_putvalue=x & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x8001013):         #SHFLI  92 
        fh.write('------------------------------------------------------------------------------------------------------------------SHFLI 92 ') 
        x=mav_putvalue_src1
        mav_putvalue=mav_putvalue_src1
        shamt_imm= imm_value & (31)
        if(shamt_imm & 16):
            x=suffle64(x, 0x0000ffff00000000,0x00000000ffff0000,16)
        if(shamt_imm & 8):
            x=suffle64(x, 0x00ff000000ff0000,0x0000ff000000ff00,8)
        if(shamt_imm & 4):
            x=suffle64(x, 0x0f000f000f000f00,0x00f000f000f000f0,4)
        if(shamt_imm & 2):
            x=suffle64(x, 0x3030303030303030,0x0c0c0c0c0c0c0c0c,2)
        if(shamt_imm & 1):
            x=suffle64(x, 0x4444444444444444,0x2222222222222222,1)
        mav_putvalue=x & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x8005013):         #UNSHFLI  93  
        fh.write('------------------------------------------------------------------------------------------------------------------UNSHFLI 93 ') 
        x=mav_putvalue_src1
        mav_putvalue=mav_putvalue_src1
        shamt= imm_value & (31)
        if(shamt & 1):
            x=suffle64(x, 0x4444444444444444,0x2222222222222222,1)
        if(shamt & 2):
            x=suffle64(x, 0x3030303030303030,0x0c0c0c0c0c0c0c0c,2)
        if(shamt & 4):
            x=suffle64(x, 0x0f000f000f000f00,0x00f000f000f000f0,4)
        if(shamt & 8):
            x=suffle64(x, 0x00ff000000ff0000,0x0000ff000000ff00,8)
        if(shamt & 16):
            x=suffle64(x, 0x0000ffff00000000,0x00000000ffff0000,16)
        mav_putvalue=x & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x800103B):         #SHFLW  94 
        fh.write('------------------------------------------------------------------------------------------------------------------SHFLW 94 ') 
        x=mav_putvalue_src1
        mav_putvalue=mav_putvalue_src1
        shamt= mav_putvalue_src2 & (31)            
        if(shamt & 8):
            x=suffle64(x, 0x00ff000000ff0000,0x0000ff000000ff00,8)
        if(shamt & 4):
            x=suffle64(x, 0x0f000f000f000f00,0x00f000f000f000f0,4)
        if(shamt & 2):
            x=suffle64(x, 0x3030303030303030,0x0c0c0c0c0c0c0c0c,2)
        if(shamt & 1):
            x=suffle64(x, 0x4444444444444444,0x2222222222222222,1)
        out=x & 0x00000000ffffffff
        mav_putvalue=sign_exe(out)
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x800503B):         #UNSHFLW  95  
        fh.write('------------------------------------------------------------------------------------------------------------------UNSHFLW 95 ') 
        x=mav_putvalue_src1
        mav_putvalue=mav_putvalue_src1
        shamt= mav_putvalue_src2 & (31)
        if(shamt & 1):
            x=suffle64(x, 0x4444444444444444,0x2222222222222222,1)
        if(shamt & 2):
            x=suffle64(x, 0x3030303030303030,0x0c0c0c0c0c0c0c0c,2)
        if(shamt & 4):
            x=suffle64(x, 0x0f000f000f000f00,0x00f000f000f000f0,4)
        if(shamt & 8):
            x=suffle64(x, 0x00ff000000ff0000,0x0000ff000000ff00,8)
        out=x & 0x00000000ffffffff
        mav_putvalue=sign_exe(out)
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x60301013):         #BMATFLIP  96  
        fh.write('------------------------------------------------------------------------------------------------------------------BMATFLIP 96 ') 
        x= bmatflip(mav_putvalue_src1)
        mav_putvalue=x & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x28105013):         #GORCI 97 
        fh.write('------------------------------------------------------------------------------------------------------------------GORCI 97 ') 
        x=mav_putvalue_src1
        mav_putvalue=mav_putvalue_src1
        if (shamt_imm & 1):
            x= x | ((x & 0x5555555555555555)<< 1) | ((x & 0xaaaaaaaaaaaaaaaa) >>1)
        if (shamt_imm & 2):
            x= x | ((x & 0x3333333333333333)<< 2) | ((x & 0xcccccccccccccccc) >>2)
        if (shamt_imm & 4):
            x= x| ((x & 0x0f0f0f0f0f0f0f0f)<< 4) | ((x & 0xf0f0f0f0f0f0f0f0) >>4)
        if (shamt_imm & 8):
            x= x | ((x & 0x00ff00ff00ff00ff)<< 8) | ((x & 0xff00ff00ff00ff00) >>8)
        if (shamt_imm & 16):
            x= x| ((x & 0x0000ffff0000ffff)<< 16) | ((x & 0xffff0000ffff0000) >>16)
        if (shamt_imm & 32):
            x=x | ((x & 0x00000000ffffffff)<< 32) | ((x & 0xffffffff00000000) >>32)
        mav_putvalue=x & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif(mav_putvalue_instr == 0x2840501B):         #GORCIW 98
        fh.write('------------------------------------------------------------------------------------------------------------------GORCIW 98 ') 
        x=mav_putvalue_src1
        mav_putvalue=mav_putvalue_src1
        if (shamt_imm & 1):
            x= x | ((x & 0x5555555555555555)<< 1) | (( x & 0xaaaaaaaaaaaaaaaa) >>1)
            mav_putvalue=x & 0x00000000ffffffff
        if (shamt_imm & 2):
            x= x | ((x & 0x3333333333333333)<< 2) | ((x & 0xcccccccccccccccc) >>2)
            mav_putvalue=x & 0x00000000ffffffff
        if (shamt_imm & 4):
            x= x| ((x & 0x0f0f0f0f0f0f0f0f)<< 4) | (( x & 0xf0f0f0f0f0f0f0f0) >>4)
            mav_putvalue=x & 0x00000000ffffffff
        if (shamt_imm & 8):
            x= x | ((x & 0x00ff00ff00ff00ff)<< 8) | ((x & 0xff00ff00ff00ff00) >>8)
            mav_putvalue=x & 0x00000000ffffffff
        if (shamt_imm & 16):
            x= x | ((x & 0x0000ffff0000ffff)<< 16) | ((x & 0xffff0000ffff0000) >>16)
            mav_putvalue=x & 0x00000000ffffffff
        if (shamt_imm & 32): 
            x= x | ((x & 0x00000000ffffffff)<< 32) | ((x & 0xffffffff00000000) >>32)
            mav_putvalue=x & 0x00000000ffffffff
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x68405013):  #GREVI  99 
        fh.write('------------------------------------------------------------------------------------------------------------------GREVI 99 ') 
        x=mav_putvalue_src1
        mav_putvalue=mav_putvalue_src1
        if (shamt_imm & 1):
            x= ((x & 0x5555555555555555)<< 1) | (( x & 0xaaaaaaaaaaaaaaaa) >>1)
        if (shamt_imm & 2):
            x= ((x & 0x3333333333333333)<< 2) | ((x & 0xcccccccccccccccc) >>2)
        if (shamt_imm & 4):
            x= ((x & 0x0f0f0f0f0f0f0f0f)<< 4) | (( x & 0xf0f0f0f0f0f0f0f0) >>4)
        if (shamt_imm & 8):
            x= ((x & 0x00ff00ff00ff00ff)<< 8) | (( x & 0xff00ff00ff00ff00) >>8)
        if (shamt_imm & 16):
            x= ((x & 0x0000ffff0000ffff)<< 16) | (( x & 0xffff0000ffff0000) >>16) 
        if (shamt_imm & 32):
            x= ((x & 0x00000000ffffffff)<< 32) | (( x & 0xffffffff00000000) >>32)
        mav_putvalue=x & 0xffffffffffffffff     
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x6810501B):  #GREVIW  100 
        fh.write('------------------------------------------------------------------------------------------------------------------GREVIW 100 ') 
        x=mav_putvalue_src1
        mav_putvalue=mav_putvalue_src1
        if (shamt_imm & 1):
            x= ((x & 0x5555555555555555)<< 1) | (( x & 0xaaaaaaaaaaaaaaaa) >>1)
        if (shamt_imm & 2):
            x= ((x & 0x3333333333333333)<< 2) | (( x & 0xcccccccccccccccc) >>2)
        if (shamt_imm & 4):
            x= ((x & 0x0f0f0f0f0f0f0f0f)<< 4) | (( x & 0xf0f0f0f0f0f0f0f0) >>4)
        if (shamt_imm & 8):
            x= ((x & 0x00ff00ff00ff00ff)<< 8) | (( x & 0xff00ff00ff00ff00) >>8)
        if (shamt_imm & 16):
            x= ((x & 0x0000ffff0000ffff)<< 16) | (( x & 0xffff0000ffff0000) >>16) 
        if (shamt_imm & 32):
            x= ((x & 0x00000000ffffffff)<< 32) | (( x & 0xffffffff00000000) >>32)
        mav_putvalue=sign_exe(x)
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x4105013):  #FSRI  101 (check)
        fh.write('------------------------------------------------------------------------------------------------------------------FSRI 101 ') 
        shamt1= fsr_imm_value & (127)
        A= mav_putvalue_src1
        B= mav_putvalue_src3
        if(shamt1>=64):
            shamt1=shamt1-64
            A= mav_putvalue_src3
            B= mav_putvalue_src1
        if(shamt1):
            mav_putvalue= (A >> shamt1) | (B << (64-shamt1))
        else:
            mav_putvalue=A
        mav_putvalue=mav_putvalue & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    elif (mav_putvalue_instr == 0x410501B):  #FSRIW  102
        fh.write('------------------------------------------------------------------------------------------------------------------FSRIW 102 ') 
        shamt1= fsr_imm_value & (63)
        src1=mav_putvalue_src1 & 0x00000000FFFFFFFF
        src3=mav_putvalue_src3 & 0x00000000FFFFFFFF
        A= src1
        B= src3
        if(shamt1>=32):
            shamt1=shamt1-32
            A= src3
            B= src1
        if(shamt1):
            mav_putvalue= (A >> shamt1) | (B << (32-shamt1))
        else:
            mav_putvalue=A
        mav_putvalue=sign_exe(mav_putvalue)
        return ((mav_putvalue<<1)|1)

    def slo(src1,src2):         #SLO function
        shamt1= src2 & (63)
        out=((src1)<< shamt1)
        res=out
        min_i=0
        max_i=shamt1
        while (min_i<max_i):
            res=((1 << min_i) | res)
            min_i=min_i+1
        mav_putvalue=res & 0xffffffff
        return mav_putvalue

    if (mav_putvalue_instr == 0x48007033):  #BFP  103 
        fh.write('------------------------------------------------------------------------------------------------------------------BFP 103 ') 
        cfg = mav_putvalue_src2 >> (32)
        leng=0
        off=0
        if((cfg>>30)==2):
            cfg = cfg>>16
        leng= (cfg>>8) & 31
        off= cfg & 63
        if leng:
            leng=leng
        else:
            leng=32
        mask = slo(0, leng) << off
        data = mav_putvalue_src2 << off
        mav_putvalue =(data & mask) | (mav_putvalue_src1 & (~mask))
        mav_putvalue=mav_putvalue & 0xffffffffffffffff
        return ((mav_putvalue<<1)|1)

    if (mav_putvalue_instr == 0x4800703B):  #BFPW  104 
        fh.write('------------------------------------------------------------------------------------------------------------------BFPW 104 ') 
        cfg = mav_putvalue_src2 >> (32)
        leng=0
        off=0
        if((cfg>>30)==2):
            cfg = cfg>>16
        leng= (cfg>>8) & 31
        off= cfg & 63
        if leng:
            leng=leng
        else:
            leng=32
        mask = slo(0, leng) << off
        data = mav_putvalue_src2 << off
        mav_putvalue =(data & mask) | (mav_putvalue_src1 & (~mask))
        mav_putvalue=mav_putvalue & 0x0000000ffffffff
        return ((mav_putvalue<<1)|1)
    def pcnt(src1):
        binary = bin(src1)  
        setBits = [ones for ones in binary[2:] if ones=='1'] 
        mav_putvalue= len(setBits) 
        return mav_putvalue

    if (mav_putvalue_instr == 0x48003033):    #BMATXOR  105
        fh.write('---------------------------------------------------------------------------------------------------------------BMATXOR 105')
        src2=bmatflip(mav_putvalue_src2)
        u=list(range(0, 8))
        v=list(range(0, 8))
        i=0
        while i<8:
            u[i]= mav_putvalue_src1 >>(i*8)
            v[i]= src2 >>(i*8)
            i=i+1     
        x=0
        j=0
        while j<64:
            y=j/8
            y=int(y)
            r=u[y] & v[j%8]
            z=pcnt(r)
            if(z&1):
                x|=(1<<j)
            j=j+1
        return ((x <<1)|1)

    if (mav_putvalue_instr == 0x8003033):    #BMATOR  106
        fh.write('---------------------------------------------------------------------------------------------------------------BMATOR 106')
        src2=bmatflip(mav_putvalue_src2)
        u=list(range(0, 8))
        v=list(range(0, 8))
        i=0
        while i<8:
            u[i]= mav_putvalue_src1 >>(i*8)
            v[i]= src2 >>(i*8)
            i=i+1     
        x=0
        j=0
        while j<64:
            y=j/8
            y=int(y)
            r=u[y] & v[j%8]
            if(r!=1):
                x|=(1<<j)
            j=j+1
        return ((x <<1)|1)


    if mav_putvalue_instr not in instr_list:
        mav_putvalue=0x00000000000000
        return ((mav_putvalue << 1)|0)
    return 0       
    
    

